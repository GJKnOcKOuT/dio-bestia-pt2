package com.gioco.sos.Gioco;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Caricatore_immagini {
BufferedImage image;

public BufferedImage carica_immagine(String posizione) {
	try {
		image= ImageIO.read(getClass().getResource(posizione));
	} catch (IOException e) {
		System.out.println("Immagine alla posizione"+posizione+"caricata correttamente");
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return image;
}
}
